<?php namespace App\Http\Controllers\Home;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Movies;
use App\Services\Search\Contract\SearchServiceContract;
use App\Services\Search\TMDbService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Stringy\Stringy;

class SearchController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @param SearchServiceContract|TMDbService $search
     * @return Response
     */
    public function store(Request $request, TMDbService $search)
    {
        $id = $request->input('id');

        $movie = Movies::exists($id);
        if ($movie->isEmpty()) {
            $movie = Movies::add($id, $search);
        } else {
            $movie = $movie->first();
        }

        $title = Stringy::create($movie->title . "-" . $movie->release_date->year . '-' . $movie->movie_id)->slugify();

        return [
            'url' => "/movie/{$title}"
        ];
    }

    /**
     * Display the specified resource.
     *
     * @param $keyword
     * @param TMDbService $search
     * @return Response
     */
    public function show($keyword, TMDbService $search)
    {
        return $search->search($keyword);
    }
}
