<?php namespace App;

use App\Services\Search\Contract\SearchServiceContract;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class Movies extends Model
{

    protected $table = 'movies';

    protected $guarded = ['id'];

    /**
     * @param $movieID
     * @return Collection
     */
    public static function exists($movieID)
    {
        return self::whereMovieId($movieID)->get();
    }

    /**
     * @param $movieId
     * @return Collection
     */
    public static function findByMovieId($movieId)
    {
        return self::whereMovieId($movieId)->first();
    }

    /**
     * @param $movieId
     * @param SearchServiceContract $search
     * @return Collection
     */
    public static function add($movieId, SearchServiceContract $search)
    {
        $id = $movieId;

        $response = $search->lookup($id);

        $movie = new Movies();
        $movie->movie_id = $response['id'];
        $movie->imdb_id = $response['imdb_id'];
        $movie->title = $response['title'];
        $movie->adult = $response['adult'];
        $movie->overview = $response['overview'];
        $movie->genres = $response['genres'];
        $movie->homepage = $response['homepage'];
        $movie->poster = $response['poster_path'];
        $movie->backdrop = $response['backdrop_path'];
        $movie->release_date = $response['release_date'];
        $movie->releases = $response['releases'];
        $movie->runtime = $response['runtime'];
        $movie->status = $response['status'];
        $movie->tagline = $response['tagline'];
        $movie->vote_average = $response['vote_average'];
        $movie->vote_count = $response['vote_count'];
        $movie->collection = $response['belongs_to_collection'];
        $movie->credits = $response['credits'];
        $movie->save();

        return self::find($movie->id);
    }

    public function setGenresAttribute($value)
    {
        $this->attributes['genres'] = Collection::make($value);
    }

    public function setCollectionAttribute($value)
    {
        $this->attributes['collection'] = Collection::make($value);
    }

    public function setReleasesAttribute($value)
    {
        $this->attributes['releases'] = Collection::make($value);
    }

    public function setCreditsAttribute($value)
    {
        $this->attributes['credits'] = Collection::make($value);
    }

    public function getReleaseDateAttribute($value)
    {
        return Carbon::createFromFormat('Y-m-d', $value);
    }

    public function getReleasesAttribute($value)
    {
        return Collection::make(json_decode($value)->countries);
    }

    public function getGenresAttribute($value)
    {
        return Collection::make(json_decode($value))->implode('name', ', ');
    }

    public function getCreditsAttribute($value)
    {
        return Collection::make(json_decode($value));
    }

}
