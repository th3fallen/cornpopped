<?php
/**
 * @author Clark Tomlinson  <clark@owncloud.com>
 *
 * @copyright Copyright (c) 2015, ownCloud, Inc.
 * @license AGPL-3.0
 */


namespace App\Services\LinkDiscovery;


use GuzzleHttp\Client;

class GuideBoxService
{
    protected $baseUrl = "http://api-public.guidebox.com/%s/US/%s/";


    public function getGuideboxDataFromTMDB($id)
    {
        $client = new Client();
        $response = $client->get($this->getBaseUrl() . 'search/movie/id/themoviedb/' . $id);

        return $this->lookupMovieById($response->json()['id']);
    }

    public function lookupMovieById($guideBoxId)
    {
        $client = new Client();
        $response = $client->get($this->getBaseUrl() . 'movie/' . $guideBoxId);

        return $response->json();
    }

    private function getBaseUrl()
    {
        $apiInformation = \Config::get('services.guidebox');

        return sprintf($this->baseUrl, $apiInformation['version'], $apiInformation['key']);
    }
}
