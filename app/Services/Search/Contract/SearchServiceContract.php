<?php
/**
 * @author Clark Tomlinson  <clark@owncloud.com>
 *
 * @copyright Copyright (c) 2015, ownCloud, Inc.
 * @license AGPL-3.0
 */


namespace App\Services\Search\Contract;


interface SearchServiceContract
{
    /**
     * @param string $keyword
     * @return mixed
     */
    public function search($keyword);

    /**
     * @param int $id
     * @return mixed
     */
    public function lookup($id);
}
