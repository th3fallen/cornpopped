<?php
/**
 * @author Clark Tomlinson  <clark@owncloud.com>
 *
 * @copyright Copyright (c) 2015, ownCloud, Inc.
 * @license AGPL-3.0
 */


namespace App\Services\Search;


use App\Services\Search\Contract\SearchServiceContract;
use GuzzleHttp\Client;

class TMDbService implements SearchServiceContract
{

    protected $baseUrl = 'https://api.themoviedb.org/3/';
    protected $apiKey = '6790bfe64526fb61fda13bf6871741fb';

    /**
     * @param string $keyword
     * @return mixed
     */
    public function search($keyword)
    {
        $client = new Client();

        $url = http_build_query([
            'query'   => $keyword,
            'api_key' => $this->apiKey
        ]);

        $result = $client->get($this->baseUrl . 'search/multi?' . $url);

        return $result->json();
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function lookup($id)
    {

        $client = new Client();

        $url = http_build_query([
            'api_key'            => $this->apiKey,
            'append_to_response' => 'releases,credits,'
        ]);

        $result = $client->get($this->baseUrl . 'movie/' . $id . '?' . $url);

        return $result->json();
    }


}
