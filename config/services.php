<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, Mandrill, and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun'  => [
        'domain' => '',
        'secret' => '',
    ],

    'mandrill' => [
        'secret' => '',
    ],

    'ses'      => [
        'key'    => '',
        'secret' => '',
        'region' => 'us-east-1',
    ],

    'stripe'   => [
        'model'  => 'App\User',
        'key'    => '',
        'secret' => '',
    ],

    'tmdb'     => [
        'service' => 'App\Services\Search\TMDbService',
        'key'     => '6790bfe64526fb61fda13bf6871741fb'
    ],

    'guidebox' => [
        'version' => 'v1.43',
        'key' => 'rKEDihCEEx8sGh35wkWLoca8vZp5ERuv'
    ]

];
