<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMoviesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movies', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('movie_id');
            $table->string('imdb_id', 100);
            $table->string('title', 100);
            $table->boolean('adult');
            $table->longText('genres');
            $table->string('homepage', 100);
            $table->longText('overview');
            $table->string('poster', '100');
            $table->string('backdrop', 100);
            $table->date('release_date');
            $table->longText('releases');
            $table->string('runtime', '100');
            $table->string('status', 100);
            $table->string('tagline', '100');
            $table->integer('vote_average');
            $table->integer('vote_count');
            $table->longText('collection');
            $table->longText('credits');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('movies');
    }

}
