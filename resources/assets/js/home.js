/*
 * Created By: Clark Tomlinson  fallen013@gmail.com
 * On: 4/25/15, 7:38 PM
 * Url: www.clarkt.com
 * Copyright Clark Tomlinson © 2015
 * 
 */

var app = angular.module('cornPopped', ['ui.bootstrap', 'cornPopped.apiServices']);

app.controller('SearchController', function ($scope, Search) {
    $scope.movie = [];
    $scope.response = [];
    $scope.search = {
        keyword: ''
    };

    $scope.findMovie = function (value) {
        return Search.get({keyword: value}).$promise.then(function (data) {
            var movies = [];
            data.results.map(function (item) {
                movies.push(item);
            });
            return movies;
        });
    };

    $scope.selectMovie = function ($item, $model, $label) {

        Search.save({id: $item.id}, function (data) {
            window.location = data.url;
        });
    };

});
