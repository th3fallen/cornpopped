/*
 * Created By: Clark Tomlinson  fallen013@gmail.com
 * On: 4/27/15, 8:23 PM
 * Url: www.clarkt.com
 * Copyright Clark Tomlinson © 2015
 * 
 */
app.controller('MovieController', function ($scope, $http) {

    // Set default state of cast collapse to true
    $scope.isCastCollapsed = true;
});
