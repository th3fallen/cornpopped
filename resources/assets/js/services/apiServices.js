/*
 * Created By: Clark Tomlinson  fallen013@gmail.com
 * On: 5/1/15, 11:42 AM
 * Url: www.clarkt.com
 * Copyright Clark Tomlinson © 2015
 * 
 */
var service = angular.module('cornPopped.apiServices', ['ngResource']);

// Search service
service.factory('Search', function ($resource) {
    return $resource(
        '/search/:keyword',
        {keyword: '@keyword'},
        {
            post: {
                method: "POST",
                isArray: false,
                headers: {'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8'}
            },
            'update': {method: "PUT"}
        }
    );
});
