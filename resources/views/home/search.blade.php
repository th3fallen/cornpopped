<form class="home-search" ng-controller="SearchController">
    <input type="text" name="search.keyword" class="form-control input-lg search" placeholder="Find your movie"
           ng-model="search.keyword" typeahead="movie.title for movie in findMovie($viewValue)"
           typeahead-editable="false"
           typeahead-min-length="3"
           typeahead-on-select="selectMovie($item, $model, $label)"
           typeahead-template-url="movieTemplate.html"
            />
    <input type="hidden" name="_token" value="{{csrf_token()}}"/>

    <div class="result">
    <pre>

        @{{ search }}
        @{{ movie }}
        @{{ response }}
    </pre>
        <span class="title">@{{ movie.title }}</span>

        <div class="backdrop">
            <img ng-src="http://image.tmdb.org/t/p/w154@{{ movie.model.poster_path }}" alt=""/>
        </div>
        <span class="release">@{{ movie.model.release_date }}</span>

        <div class="votes">@{{ movie.model.vote_average }}</div>
</form>

<script type="text/ng-template" id="movieTemplate.html">
    <span class="title">@{{ match.label}}</span>
    <div class="backdrop">
        <img ng-src="http://image.tmdb.org/t/p/w154@{{ match.model.poster_path }}" alt=""/>
    </div>
</script>


</div>
