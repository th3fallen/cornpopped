@extends('app')
@section('content')
    {{--<style type="text/css">--}}
    {{--body{--}}
    {{--background-image: url(http://image.tmdb.org/t/p/original{{ $movie->backdrop}});--}}
    {{--background-repeat: no-repeat;--}}
    {{--}--}}
    {{--</style>--}}
    <section id="about" ng-controller="MovieController">

        <div class="col-lg-6 wp2 delay-1s animated fadeInUp">
            <div class="cover">
                <img src="http://image.tmdb.org/t/p/w154{{ $movie->poster }}" alt=""/>
            </div>
            <h2>{{$movie->title}}</h2>
            <h4 class="sub-text">Release Date: {{$movie->release_date->format('M d, Y')}}<br>
                Rating: {{ $movie->releases->where('iso_3166_1', 'US')->first()->certification }} <br>
            </h4>


            <table class="text-dark">

                <tbody>
                <tr>
                    <td>Year</td>
                    <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                    <td>{{$movie->release_date->format('M d, Y')}}</td>
                </tr>

                <tr>
                    <td>Slogan</td>
                    <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                    <td>{{ $movie->tagline }}</td>
                </tr>

                <tr>
                    <td>Genre</td>
                    <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                    <td>{{$movie->genres}}</td>
                </tr>

                <tr>
                    <td>Time</td>
                    <td>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;</td>
                    <td>{{ $movie->runtime }} min. / {{ gmdate('H:i', $movie->runtime*60)  }}</td>
                </tr>

                </tbody>
            </table>
            <br>


            <p>{{ $movie->overview }}</p>

            <div class="detailed">
                Rating:&nbsp;&nbsp;
                @for($i = 1; $i <= $movie->vote_average; $i++)
                    <i class="fa fa-star"></i>
                @endfor
                <span>TMDb &nbsp;<b>/ {{ $movie->vote_average }}</b></span>

                <br/>
                <button class="btn btn-primary" ng-click="isCastCollapsed = !isCastCollapsed">@{{ isCastCollapsed ? 'Show' : 'Hide' }} Cast</button>

                <div class="cast row" collapse="isCastCollapsed">
                    @foreach($movie->credits->get('cast') as $cast)
                        <div class="cast-member col-md-4">
                            <div class="name">{{$cast->name}}</div>
                        <span class="profile-picture">
                            <img src="http://image.tmdb.org/t/p/w154{{$cast->profile_path}}" alt=""/>
                        </span>
                        </div>
                    @endforeach
                </div>

            </div>
        </div>
    </section>

@endsection
